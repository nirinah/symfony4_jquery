<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\UserType;
use App\Entity\User;


class SearchController extends AbstractController
{
    /**
     * @Route("/search/user", name="search_user")
     */
    public function searchUser()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $list = $this->getDoctrine()->getRepository(User::class)->findAll();
        dump($list);
        return $this->render('search/index.html.twig');
    }

    /** 
     * @Route("/user/list/search", name="search_list_user")
     */
    public function listDataSearch(Request $request)
    {
        $obj = json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository(User::class)->findAll(); //getuserByName($obj->name);
        $data = [];

        if (!empty($obj->name)) {
	        foreach ($list as $key => $value) {
	        	if (preg_match('/^'.$obj->name.'/i', $value->getFirstname())) {
		             $data[] = [
		                'firstname'  =>   $value->getFirstname(),
		                'lastname'   =>   $value->getLastname(),
		                'email'      =>   $value->getEmail(),
		            ];
		        }
	        }
	    }

        return new JsonResponse($data, 200);
    }

     /**
     * @Route("/new/user", name="new_user")
     */
    public function createNewUser(Request $request)
    {
        $user = new User();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $entityManager->persist($user);
             dump($user);die;
            $entityManager->flush();

            return $this->redirectToRoute('search_user');           

        }
        return $this->render('user/create.html.twig',['form' => $form->createView(),]);
    }
}
