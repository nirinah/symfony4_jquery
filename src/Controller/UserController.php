<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use app\services\UserService;
use App\Entity\User;


class UserController extends AbstractController
{
	/**
     * @Route("/api/create/user", name="create_user")
     */
    public function createUser(Request $request, UserService $userService)
    {
    	$data = json_decode($request->getContent());
    	$obj = $userService->serializerUser();
    	$response = new Response();
    	$response->setContent($obj);
    	$response->headers->set('Content-Type', 'application/json; charset=UTF-8');
        $response->headers->set('Content-Type', 'application/json', 'multipart/form-data', 'text/plain');
        $response->headers->set('Access-Control-Allow-Headers', 'origin, content-type, accept, Authorization, Lang, token');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');

    	return $response;
    	//dump($data);die;
    }
}