<?php

namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Entity\User;

 
class UserNormalizer implements NormalizerInterface
{
	
	 /**
     * @param User $user
     * @param null               $format
     * @param array              $context
     *
     * @return array
     */
    public function normalize($user, $format = null, array $context = [])
    {
    	$data['name'] = 'JEAN';
    	$data['adress'] = 'Tana';
    	$data['societe'] = 'AXE';

    	return $data;
    }

     /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof User;
    }
}