<?php

namespace App\manager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer;


class CommonManager
{
	
	/**
     * Serialize list.
     *
     * @param NormalizerInterface $normalizer
     * @param array               $list
     * @param string              $format
     * @param array               $context
     *
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    public function serializeList(
        NormalizerInterface $normalizer,
        array $list,
        $format = 'json',
        array $context = array()
    ) {
        $encoder = new JsonEncoder();
        $serializer = new Serializer(array($normalizer), array($encoder));

        return $serializer->serialize($list, $format, $context);
    }
}