<?php

namespace App\services;
use App\manager\CommonManager;
use App\Normalizer\UserNormalizer;


class UserService extends CommonManager
{
	private $userNormalizer;

	public function __construct(UserNormalizer $userNormalizer)
	{
		$this->userNormalizer = $userNormalizer;
	}
	
	public function serializerUser()
	{
		return $this->serializeList($this->userNormalizer,'json');
	}
}